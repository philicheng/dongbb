package com.zimug.dongbb.server.jwt.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zimug.commons.exception.CustomException;
import com.zimug.commons.exception.CustomExceptionType;


import com.zimug.dongbb.persistence.system.mapper.MySystemMapper;
import com.zimug.dongbb.persistence.system.mapper.SysRoleMapper;
import com.zimug.dongbb.persistence.system.mapper.SysUserRoleMapper;
import com.zimug.dongbb.persistence.system.model.SysDict;
import com.zimug.dongbb.persistence.system.model.SysRole;
import com.zimug.dongbb.persistence.system.model.SysRoleApi;
import com.zimug.dongbb.persistence.system.model.SysUserRole;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysroleService {

  @Resource
  private SysRoleMapper sysRoleMapper;
  @Resource
  private MySystemMapper mySystemMapper;
  @Resource
  private SysUserRoleMapper sysUserRoleMapper;

  public List<SysRole> queryRoles(String roleLik) {
      /*SysRoleExample sysRoleExample = new SysRoleExample();
      if(StringUtils.isNotEmpty(roleLik)){
        sysRoleExample.or().andRoleCodeLike("%"+ roleLik+ "%");
        sysRoleExample.or().andRoleDescLike("%"+ roleLik+ "%");
        sysRoleExample.or().andRoleNameLike("%"+ roleLik+ "%");
      }
      return sysRoleMapper.selectByExample(sysRoleExample);*/

    QueryWrapper<SysRole> query = new QueryWrapper<>();
    query.like(StringUtils.isNotEmpty(roleLik),"role_code",roleLik)
      .or()
      .like(StringUtils.isNotEmpty(roleLik),"role_desc",roleLik)
      .or()
      .like(StringUtils.isNotEmpty(roleLik),"role_name",roleLik);

    return sysRoleMapper.selectList(query);
  }

  public void updateRole(SysRole sysrole){
    if(sysrole.getId() == null){
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "修改操作必须带主键");
    }else{
      sysRoleMapper.updateById(sysrole);
    }
  }

  public void addRole(SysRole sysrole){
    sysRoleMapper.insert(sysrole);
  }

  public void deleteRole(Integer roleId){
    sysRoleMapper.deleteById(roleId);
  }

  public Map<String,Object> getRolesAndChecked(Long userId){
    Map<String,Object> ret = new HashMap<>();
    if(userId == null){
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "获取角色信息必需要：用户id参数");
    }else{
      ret.put("checkedRoleIds",mySystemMapper.getCheckedRoleIds(userId));
      ret.put("roleDatas",sysRoleMapper.selectList(null));
    }
    return ret;
  }


  @Transactional
  public void saveCheckedKeys(Long userId,List<Long> checkedIds){

    /*SysUserRoleExample sysUserRoleExample = new SysUserRoleExample();
    sysUserRoleExample.createCriteria().andUserIdEqualTo(userId);
    sysUserRoleMapper.deleteByExample(sysUserRoleExample);*/


    sysUserRoleMapper.delete(new UpdateWrapper<SysUserRole>().eq("user_id",userId));

    mySystemMapper.insertUserRoleIds(userId,checkedIds);
  }
}
