package com.zimug.dongbb.server.jwt.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zimug.dongbb.persistence.system.mapper.SysDictMapper;
import com.zimug.dongbb.persistence.system.model.SysDict;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 数据字典表服务层代码
 * @author 字母哥
 */
@Service
public class SysDictService {

  @Resource
  private SysDictMapper sysDictMapper;

  /**
   * 查询所有
   */
  public List<SysDict> all(){
    return  sysDictMapper.selectList(null);
  }

  /**
   * 根据参数查询
   * @param dictLik 查询参数
   */
  public List<SysDict> queryDicts(String dictLik) {
    QueryWrapper<SysDict> query = new QueryWrapper<>();
    query.like(StringUtils.isNotEmpty(dictLik),"group_name",dictLik)
      .or()
      .like(StringUtils.isNotEmpty(dictLik),"group_code",dictLik);

    return sysDictMapper.selectList(query);
  }

  /**
   * 根据id更新数据字典项
   * @param sysDict 更新实体(包含id)
   */
  public void updateDict(SysDict sysDict){
    Assert.isTrue(sysDict.getId() != null,
      "更新数据必须指定数据更新条件（主键）");

    sysDictMapper.updateById(sysDict);
  }

  /**
   * 新增数据字典项
   * @param sysDict 新增实体
   */
  public void addDict(SysDict sysDict){
    sysDict.setCreateTime(new Date());
    sysDictMapper.insert(sysDict);
  }

  /**
   * 根据id删除数据字典项
   * @param id  删除项的id
   */
  public void deleteDict(Long id){
    Assert.isTrue(id != null,
      "删除数据必须指定数据删除条件（主键）");
    sysDictMapper.deleteById(id);
  }

}
