package com.zimug.dongbb.server.jwt.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zimug.commons.model.tree.DataTreeUtil;
import com.zimug.commons.exception.CustomException;
import com.zimug.commons.exception.CustomExceptionType;
import com.zimug.dongbb.persistence.system.mapper.MySystemMapper;
import com.zimug.dongbb.persistence.system.mapper.SysMenuMapper;
import com.zimug.dongbb.persistence.system.mapper.SysRoleMenuMapper;
import com.zimug.dongbb.persistence.system.model.SysApi;
import com.zimug.dongbb.persistence.system.model.SysMenu;
import com.zimug.dongbb.persistence.system.model.SysRoleApi;
import com.zimug.dongbb.persistence.system.model.SysRoleMenu;
import com.zimug.dongbb.server.jwt.system.model.SysMenuNode;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SysmenuService {

  @Resource
  private MySystemMapper mySystemMapper;
  @Resource
  private SysMenuMapper sysMenuMapper;
  @Resource
  private SysRoleMenuMapper sysRoleMenuMapper;

  public List<SysMenuNode> getMenuTree(String menuNameLike,
                                           Boolean menuStatus) {
    //查找level=1的菜单节点，即：根节点
    /*SysMenuExample sysMenuExample = new SysMenuExample();
    sysMenuExample.createCriteria().andLevelEqualTo(1);
    List<SysMenu> rootMenus = sysMenuMapper.selectByExample(sysMenuExample);*/

    SysMenu rootSysMenu = sysMenuMapper.selectOne(
      new QueryWrapper<SysMenu>().eq("level",1));


    if (rootSysMenu != null) {
      Long rootMenuId = rootSysMenu.getId();

      List<SysMenu> sysMenus
        = mySystemMapper.selectMenuTree(rootMenuId, menuNameLike, menuStatus);

      List<SysMenuNode> sysMenuNodes = sysMenus.stream().map(item -> {
        SysMenuNode bean = new SysMenuNode();
        BeanUtils.copyProperties(item, bean);
        return bean;
      }).collect(Collectors.toList());

      if (StringUtils.isNotEmpty(menuNameLike) || menuStatus != null) {
        return sysMenuNodes;//根据菜单名称查询，返回平面列表
      } else {//否则返回菜单的树型结构列表
        return DataTreeUtil.buildTree(sysMenuNodes, rootMenuId);
      }

    } else {
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "找不到菜单根节点，请先去创建");
    }
  }


  public void updateMenu(SysMenu sysmenu){
    if(sysmenu.getId() == null){
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "修改操作必须带主键");
    }else{
      sysMenuMapper.updateById(sysmenu);
    }
  }

  @Transactional
  public void addMenu(SysMenu sysmenu){
    setMenuIdsAndLevel(sysmenu);

    sysmenu.setIsLeaf(true); //新增的菜单节点都是子节点，没有下级
    SysMenu parent = new SysMenu();
    parent.setId(sysmenu.getMenuPid());
    parent.setIsLeaf(false);//更新父节点为非子节点。
    sysMenuMapper.updateById(parent);

    sysmenu.setStatus(false);//设置是否禁用，新增节点默认可用
    sysMenuMapper.insert(sysmenu);
  }


  @Transactional
  public void deleteMenu(SysMenu sysMenu){
    //查找被删除节点的子节点
    /*SysMenuExample sysMenuExample = new SysMenuExample();
    sysMenuExample.createCriteria().andMenuPidsLike("%["+ sysMenu.getId() +"]%");
    List<SysMenu> myChilds = sysMenuMapper.selectByExample(sysMenuExample);*/

    List<SysMenu> myChilds = sysMenuMapper.selectList(new QueryWrapper<SysMenu>()
      .like("menu_pids","["+ sysMenu.getId() +"]"));

    if(myChilds.size() > 0){
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "不能删除含有下级菜单的菜单");
    }

    //查找被删除节点的父节点
    /*sysMenuExample = new SysMenuExample();
    sysMenuExample.createCriteria().andMenuPidsLike("%["+ sysMenu.getMenuPid() +"]%");
    List<SysMenu> myFatherChilds = sysMenuMapper.selectByExample(sysMenuExample);*/

    List<SysMenu> myFatherChilds = sysMenuMapper.selectList(new QueryWrapper<SysMenu>()
      .like("menu_pids","["+ sysMenu.getMenuPid() +"]"));

    //我的父节点只有我这一个子节点，而我还要被删除，更新父节点为叶子节点。
    if(myFatherChilds.size() == 1){
      SysMenu parent = new SysMenu();
      parent.setId(sysMenu.getMenuPid());
      parent.setIsLeaf(true);//更新父节点为叶子节点。
      sysMenuMapper.updateById(parent);
    }
    //删除节点
    sysMenuMapper.deleteById(sysMenu.getId());
  }

  //设置某子节点的所有祖辈id
  private void setMenuIdsAndLevel(SysMenu child){
    List<SysMenu> allMenus = sysMenuMapper.selectList(null);
    for(SysMenu sysMenu: allMenus){
      //从组织列表中找到自己的直接父亲
      if(sysMenu.getId().equals(child.getMenuPid())){
        //直接父亲的所有祖辈id + 直接父id = 当前子节点的所有祖辈id
        //爸爸的所有祖辈 + 爸爸 = 孩子的所有祖辈
        child.setMenuPids(sysMenu.getMenuPids() + ",[" + child.getMenuPid() + "]" );
        child.setLevel(sysMenu.getLevel() + 1);
      }
    }
  }

  public List<String> getCheckedKeys(Long roleId){
    return mySystemMapper.selectMenuCheckedKeys(roleId);
  }
  public List<String> getExpandedKeys(){
    return mySystemMapper.selectMenuExpandedKeys();
  }

  @Transactional
  public void saveCheckedKeys(Long roleId,List<Long> checkedIds){
    /*SysRoleMenuExample sysRoleMenuExample = new SysRoleMenuExample();
    sysRoleMenuExample.createCriteria().andRoleIdEqualTo(roleId);
    sysRoleMenuMapper.deleteByExample(sysRoleMenuExample);*/

    sysRoleMenuMapper.delete(new UpdateWrapper<SysRoleMenu>().eq("role_id",roleId));

    mySystemMapper.insertRoleMenuIds(roleId,checkedIds);
  }


  public List<SysMenuNode> getMenuTreeByUsername(String username) {
    List<SysMenu> sysMenus = mySystemMapper.selectMenuByUsername(username);

    if (sysMenus.size() > 0) {
      Long rootMenuId = sysMenus.get(0).getId();

      List<SysMenuNode> sysMenuNodes =
        sysMenus.stream().map(item -> {
          SysMenuNode bean = new SysMenuNode();
          BeanUtils.copyProperties(item, bean);
          return bean;
        }).collect(Collectors.toList());
      return DataTreeUtil.buildTreeWithoutRoot(sysMenuNodes, rootMenuId);
    }
    return new ArrayList<>();
  }
}
