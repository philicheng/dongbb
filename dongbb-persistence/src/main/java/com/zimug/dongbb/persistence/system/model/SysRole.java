package com.zimug.dongbb.persistence.system.model;

import lombok.Data;

/**
 * sys_role
 * @author
 */
@Data
public class SysRole {
    private Long id;

    /**
     * 角色名称(汉字)
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDesc;

    /**
     * 角色的英文code.如：ADMIN
     */
    private String roleCode;

    /**
     * 角色顺序
     */
    private Integer sort;

    /**
     * 0表示可用
     */
    private Boolean status;


}