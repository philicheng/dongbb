package com.zimug.dongbb.persistence.system.model;

import lombok.Data;

import java.util.Date;


@Data
public class SysDict {

    //
    private Long id;
    //分组名称
    private String groupName;
    //分组编码
    private String groupCode;
    //字典项名称
    private String itemName;
    //字典项Value
    private String itemValue;
    //字典项描述
    private String itemDesc;
    //字典项创建时间
    private Date createTime;

}