package com.zimug.dongbb.persistence.system.model;

import lombok.Data;

import java.util.Date;

/**
 * sys_config
 * @author
 */
@Data
public class SysConfig  {
    private Long id;

    /**
     * 参数名称(中文)
     */
    private String paramName;

    /**
     * 参数唯一标识(英文及数字)
     */
    private String paramKey;

    /**
     * 参数值
     */
    private String paramValue;

    /**
     * 参数描述备注
     */
    private String paramDesc;

    /**
     * 创建时间
     */
    private Date createTime;


}