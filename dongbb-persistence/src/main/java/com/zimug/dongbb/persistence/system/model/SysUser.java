package com.zimug.dongbb.persistence.system.model;

import lombok.Data;

import java.util.Date;

/**
 * sys_user
 * @author
 */
@Data
public class SysUser {
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 组织id
     */
    private Long orgId;

    /**
     * 0无效用户，1是有效用户
     */
    private Boolean enabled;

    /**
     * 手机号
     */
    private String phone;

    /**
     * email
     */
    private String email;

    /**
     * 用户创建时间
     */
    private Date createTime;


}