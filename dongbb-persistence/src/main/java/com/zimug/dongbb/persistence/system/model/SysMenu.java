package com.zimug.dongbb.persistence.system.model;

import lombok.Data;

/**
 * sys_menu
 * @author
 */
@Data
public class SysMenu  {
    private Long id;

    /**
     * 父菜单ID
     */
    private Long menuPid;

    /**
     * 当前菜单所有父菜单
     */
    private String menuPids;

    /**
     * 0:不是叶子节点，1:是叶子节点
     */
    private Boolean isLeaf;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 跳转URL
     */
    private String url;

    private String icon;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 菜单层级
     */
    private Integer level;

    /**
     * 0:启用,1:禁用
     */
    private Boolean status;


}